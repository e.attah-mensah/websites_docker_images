## Suggested steps
1. Get [Docker Desktop](https://www.docker.com/products/docker-desktop)
2. Build base image. It's advisable to include a name and tag eg Exectue `docker build --tag wt_apache_image:1.0 .` from a directory containing the Dockerfile
3. Start a container, specifying a local host port over which to display content. eg `docker run --publish 8000:80 --detach --name wt_apache_container wt_apache_image:1.0` (Or     use the gui :smiley:)
4. Visit the site in your browser using the specified port (in this case at `localhost:80`)
5. SSH into the container via `docker exec -it <container name> /bin/bash` or using the gui's CLI button
6. Proceed with computer hacking

## More suggested steps
1. Documentation on persisting data outside of the container [here](https://docs.docker.com/storage/)