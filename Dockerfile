# Use the official debian:slim image as parent image
FROM debian:stable-slim

# Set the working directory. TODO: Explore creating and using a non-root user
WORKDIR /root

# apt update & upgrade
RUN apt update && apt upgrade -y

# install git
RUN apt install -y git

# get dotfiles
RUN git clone https://gitlab.com/e.attah-mensah/blank_12_radish.git dotfiles

# run sudo setup
RUN sh dotfiles/setup/sudo.sh

# run user setup
RUN sh dotfiles/setup/user.sh

# get site source
RUN git clone https://gitlab.com/eyeo/websites/web.adblockplus.org.git

# Create document root
RUN mkdir -p /var/www/adblockplus.org/public

# Copy virtual host file to apache's sites-available and enable it
RUN cp /root/web.adblockplus.org/adblockplus.org.conf /etc/apache2/sites-available/
RUN a2ensite adblockplus.org.conf
# Disable default site
RUN a2dissite 000-default.conf

# Build site and place output in virtual hosts document root
WORKDIR .gitlab/cms/
RUN python -m cms.bin.generate_static_pages ~/web.adblockplus.org /var/www/adblockplus.org/public

# Change working directory to site source
WORKDIR /root/web.adblockplus.org

# Start apache process in the foreground
CMD ["apachectl", "-D", "FOREGROUND"]

# Expose http port
EXPOSE 80
